# README #

Please find test cases for the project here:
https://docs.google.com/spreadsheets/d/1u8XAm2ABqwxsfU4fijpRtI6Cw0wS5Em1DYiCfpEQfOI/edit?usp=sharing

### What is this repository for? ###

* This repository contains the test cases for the assignment given related to the interview process

### Pre-requisite
* Node is installed on the system

### How do I get set up? ###

* Clone the project and navigate to the cloned directory:
```
git clone https://ayushgoel91@bitbucket.org/ayushgoel91/manatal-test.git
```

* Download node dependencies
```
npm install
```

* Run tests using cypress run
```
./node_modules/.bin/cypress run
```