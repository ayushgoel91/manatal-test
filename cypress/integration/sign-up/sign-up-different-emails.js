import constantData from '/cypress/fixtures/constantData.js'
import fillFormData from '/cypress/fixtures/fillFormData.js'

before(() => {
    cy.visit("/signup");
});

describe("Test ID - 5: Sign Up page test cases - different email and confitm email", () => {
    it("Verify error message when dfferent emails are given", () => {
        
        // Used reload instead of page visit again to make use of cookies
        cy.reload();

        // Fill form data and then add different email in confirm email address text box
        fillFormData();
        cy.get(constantData.confirmEmailId).clear().type(constantData.diffCompanyEmail);

        // Page should contain different email error
        cy.contains(constantData.diffEmailError);
    });

    it("Test ID - 6: Verify validation on email address field", () => {
        
        // Reload page
        cy.reload();

        // Enter invalid email in email field
        cy.get(constantData.companyEmailId).type(constantData.invalidEmail);

        // Page should contain invalid email error
        cy.contains(constantData.invalidEmailError);
    })
});