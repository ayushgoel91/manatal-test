import constantData from '/cypress/fixtures/constantData.js'
import fillFormData from '/cypress/fixtures/fillFormData.js'

// Open signup page
before(() => {
    cy.visit("/signup");
});

describe("Test ID - 3: Sign Up page test cases - required fields", () => {
    it("Verify required fields - Name", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Clear name field
        cy.get(constantData.nameId).clear();

        // Page should contain name error
        cy.contains(constantData.nameError);
    });

    it("Test ID - 3: Verify required fields - Organization name", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Clear org name field
        cy.get(constantData.orgNameId).clear();

        // Page should contain name error
        cy.contains(constantData.orgNameError);
    });

    it("Test ID - 3: Verify required fields - Email address", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Clear company email field
        cy.get(constantData.companyEmailId).clear();

        // Page should contain company email error
        cy.contains(constantData.companyEmailError);
    });

    it("Test ID - 3: Verify required fields - Confirm email address", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Clear confirm company email field
        cy.get(constantData.confirmEmailId).clear();
        
        // Page should contain confirm company email error
        cy.contains(constantData.confirmCompanyEmailError);
    });

    it("Test ID - 3: Verify required fields - Password", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Clear password field
        cy.get(constantData.passwordId).clear();
        
        // Page should contain password error
        cy.contains(constantData.passwordError);
    });

    it("Test ID - 3: Verify required fields - Terms and conditions", () => {
        
        // Reload and fill form data
        cy.reload();
        fillFormData();

        // Uncheck terms and conditions field
        cy.get(constantData.agreeCheckboxId).uncheck();
        
        // Page should contain accept terms and conditions error
        cy.contains(constantData.tncError);
    });

    it("Test ID - 3: Verify required fields - Leaving all fields empty", () => {
        
        // Reload page and click on Sign up button
        cy.reload();
        cy.contains("Sign Up", {
            matchCase: false
        }).click();

        // Page should contain required field errors for all fields
        cy.contains(constantData.nameError);
        cy.contains(constantData.orgNameError);
        cy.contains(constantData.companyEmailError);
        cy.contains(constantData.confirmCompanyEmailError);
        cy.contains(constantData.passwordError);
        cy.contains(constantData.tncError);
    });
});