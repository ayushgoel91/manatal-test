import constantData from '/cypress/fixtures/constantData.js'
import fillFormData from '/cypress/fixtures/fillFormData.js'

before(() => {
    cy.visit("/signup");
});

describe("Test ID - 1: Sign Up page test cases - Happy path", () => {
    it("Verify UI", () => {
        cy.contains(constantData.startFreeTrial);
        cy.contains(constantData.periodInformation);
        cy.contains(constantData.getStarted);
        cy.contains(constantData.nameField);
        cy.contains(constantData.orgNamefield);
        cy.contains(constantData.iWorkForField);
        cy.contains(constantData.companyEmailField);
        cy.contains(constantData.confirmCompanyEmailField);
        cy.contains(constantData.passwordField);
        cy.contains(constantData.phoneNumberField);
        cy.contains(constantData.tncField);
    });

    it("Test ID - 2: Verify confirmation page", () => {
        // Fill form data and click on sign up
        var email = fillFormData();
        cy.contains("Sign Up", {
            matchCase: false
        }).click();
        
        // User should land on confirmation page and confirm email text should be displayed
        cy.contains(constantData.confirmEmailText);
        cy.get(constantData.emailClassOnConfirmPage).contains(email);
    });
});