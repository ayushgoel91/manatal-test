import constantData from '/cypress/fixtures/constantData.js'

export default function fillFormData() {
    var randomNum = Math.floor(Math.random() * 1000000000);
    var email = constantData.companyEmail.concat(randomNum, "@", constantData.companyDomain)

    cy.get(constantData.nameId).type(constantData.testName)
    cy.get(constantData.orgNameId).type(constantData.testOrgName)
    cy.get(constantData.companyEmailId).type(email)
    cy.get(constantData.confirmEmailId).type(email)
    cy.get(constantData.passwordId).type(constantData.password)
    cy.get(constantData.agreeCheckboxId).check()

    return email
}