export default {
  // Element locators
  nameId: "#name",
  orgNameId: "#organization_name",
  agencyOptionId: "#agency",
  companyOptionId: "#company",
  companyEmailId: "#company_email_address",
  confirmEmailId: "#confirm_company_email_address",
  passwordId: "#password",
  agreeCheckboxId: "#agree",

  // Test data for form submission
  testName: "Ayush Goel",
  testOrgName: "Manatal",
  companyEmail: "ayush.goel",
  diffCompanyEmail: "different.email@candidate.manatal.com",
  companyDomain: "candidate.manatal.com",
  password: "test123",
  invalidEmail: "ayush.com",
  
  // Class locators
  errorClass: ".red--text",
  emailClassOnConfirmPage: ".pa-2",

  // Error texts
  nameError: "The name field is required",
  orgNameError: "The organization name field is required",
  companyEmailError: "The company email address field is required",
  confirmCompanyEmailError: "The confirm company email address field is required",
  passwordError: "The password field is required",
  tncError: "The agree field is required",
  diffEmailError: "The confirm company email address confirmation does not match",
  invalidEmailError: "The email address field must be a valid email",

  // Confirmation page
  confirmEmailText: "Confirm your e-mail address",

  // Sign up page form texts
  startFreeTrial: "Start Your Free Trial",
  periodInformation: "14-day free trial and no credit card required.",
  getStarted: "Get started in less than 20 seconds!",
  nameField: "Name *",
  orgNamefield: "Organization Name *",
  iWorkForField: "I work for * ",
  companyEmailField: "Company Email Address *",
  confirmCompanyEmailField: "Confirm Company Email Address *",
  passwordField: "Password *",
  phoneNumberField: "Phone Number",
  tncField: "I agree to the privacy policy and terms and conditions"
}